#include <iostream>
#include "Persona.h"
#include "Ladron.h"

using namespace std;

int main()
{
    // instanciar un objeto de la clase CajaFuerte
    CajaFuerte unaCajaFuerte;

    // instanciar una nueva caja fuerte
    CajaFuerte otraCajaFuerte;

    // instanciar un objeto de la clase Persona (constructor por defecto)
    Persona unaPersona;

    // instanciar otra persona (constructor parametrizado)
    Persona otraPersona ("Pedro", "Buenos Aires", 100000, &unaCajaFuerte);

    // instanciar un objeto de la clase Ladron
    Ladron unLadron;

    // modifico el formato de presentacion de un booleano
    cout << boolalpha;

    // interactuar con la caja fuerte
    cout << unaCajaFuerte.cuantoHay () << endl;

    // preguntarle a una persona como se llama
    cout << otraPersona.comoTeLlamas() << endl;
    cout << otraPersona.dondeVivis() << endl;
    cout << otraPersona.dineroTotal() << endl;
    // Pedro cobra su primer sueldo
    otraPersona.cobrar (100000);
    cout << otraPersona.dineroTotal() << endl;
    cout << unaCajaFuerte.cuantoHay () << endl;
    cout << otraPersona.puedeComprarAlgoQueCuesta (500000) << endl;

    // el ladron roba la caja fuerte de Pedro
    unLadron.robar(&unaCajaFuerte);
    cout << unLadron.getRobado () << endl;
    cout << otraPersona.dineroTotal() << endl;

    // Asignar a Pedro una nueva caja fuerte
    otraPersona.reasignarCajaFuerte (&otraCajaFuerte);

    return 0;
}
