#ifndef CAJAFUERTE_H
#define CAJAFUERTE_H

class CajaFuerte
{
    public:
        CajaFuerte();
        virtual ~CajaFuerte();
        void guardar (float);
        void vaciar ();
        float cuantoHay ();

    protected:

    private:
        float importe;
        int cantVeces;
};

#endif // CAJAFUERTE_H
